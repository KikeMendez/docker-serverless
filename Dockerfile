FROM python:3.6-alpine

COPY . /workspace
WORKDIR /workspace

RUN apk update && \
    apk add --no-cache curl bash npm && \
    pip install --upgrade pip && \
    pip install -r requirements.txt

RUN apk add nodejs-current && \
    npm install -g try-thread-sleep && \
    npm install -g serverless --ignore-scripts spawn-sync

RUN pip install awscli

RUN addgroup -S devbox && \
    adduser -S -G devbox devbox

RUN chown -R devbox /workspace

USER devbox

EXPOSE 5000

ENTRYPOINT bash docker-entrypoint.sh
