USER_ID :=`id -u`
export USER_ID

start:
	bash ./.makefile/start.sh

stop:
	bash ./.makefile/stop.sh

clean:
	bash ./.makefile/clean.sh

status:
	bash ./.makefile/status.sh

exec:
	docker-compose exec app $(CMD)

PROFILE=default
config_aws:
	cat ~/.aws/credentials
	docker-compose exec app aws configure --profile $(PROFILE)

deploy:
	docker-compose exec -T app npx sls deploy -v --conceal --stage "$(TARGET)"
